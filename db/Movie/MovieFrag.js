const { MovieFrags }  = require('../db')

function MovieFragDB() {
    
}

MovieFragDB.prototype = {
    constructor: MovieFragDB,

    create: async function({ id = undefined, movieid, fragmenturl}) {
        return await MovieFrags.create({
            id,
            movieid,
            fragmenturl
        })
    },

    deleteById: async function(id) {
        return await MovieFrags.destroy({
            where: {
                id: id
            }
        })
    },

    deleteByMovieId: async function (movieid) {
        return await MovieFrags.destroy({
            where: {
                movieid: movieid
            }
        })
    },

    updateById: async function(id, { movieid = undefined, fragmenturl = undefined }) {
        return await MovieFrags.update({
            movieid,
            fragmenturl
        }, {
            where: {
                id: id
            }
        })
    },

    findById: async function (id) {
        return await MovieFrags.findOne({
            where: {
                id: id
            }
        })
    },

    findByMovieid: async function (movieid) {
        return await MovieFrags.findAll({
            where: {
                movieid: movieid
            }
        })
    }

}

module.exports = new MovieFragDB()

