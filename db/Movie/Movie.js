const { Movies, MovieAuthors, MovieFrags, MovieUrls, MovieTags }  = require('../db')
const { createUUID } = require('../../utils/utils')

function MovieDB() {
    
}

MovieDB.prototype = {
    constructor: MovieDB,

    ORDER_CREATEDAT_ASC: ['createdAt', 'ASC'],
    ORDER_CREATEDAT_DESC: ['createdAt', 'DESC'],

    ORDER_UPDATEDAT_ASC: ['updatedAt', 'ASC'],
    ORDER_UPDATEDAT_DESC: ['updatedAt', 'DESC'],

    ORDER_TRANSAT_ASC: ['transedAt', 'ASC'],
    ORDER_TRANSAT_DESC: ['transedAt', 'DESC'],

    ORDER_TIME_ASC: ['time', 'ASC'],
    ORDER_TIME_DESC: ['time', 'DESC'],

    create: async function(movie) {
        return await Movies.create(movie)
    },

    deleteById: async function(id) {
        return await Movies.destroy({
            where: {
                id: id
            }
        })
    },

    updateById: async function(id, movie = {}) {
        return await Movies.update(movie, {
            where: {
                id: id
            }
        })
    },

    updateByHash: async function(hash, movie = {}) {
        return await Movies.update(movie, {
            where: {
                hash: hash
            }
        })
    },

    find: async function ({ orders = [], offset = undefined, limit = undefined } = {}) {
        return await Movies.findAll({
            order: orders,
            offset,
            limit
        })
    },

    findById: async function (id) {
        return await Movies.findOne({
            where: {
                id: id
            }
        })
    },

    findByHash: async function (hash) {
        return await Movies.findOne({
            where: {
                hash: hash
            }
        })
    },

    query: async function ({ orders = [], offset = undefined, limit = undefined } = {}) {
        return await Movies.findAll({
            include: [
                {
                    model: MovieUrls
                },
                {
                    model: MovieAuthors
                },
                {
                    model: MovieFrags
                },
                {
                    model: MovieTags
                }
            ],
            order: orders,
            offset,
            limit
        })
    },


    queryById: async function (id) {
        return await Movies.findOne({
            where: {
                id: id
            },
            include: [
                {
                    model: MovieUrls
                },
                {
                    model: MovieAuthors
                },
                {
                    model: MovieFrags
                },
                {
                    model: MovieTags
                }
            ]
        })
    },


    queryByStatus: async function (status, { orders = [], offset = undefined, limit = undefined } = {}) {
        return await Movies.findAll({
            include: [
                {
                    model: MovieUrls,
                    where: {
                        status
                    }
                },
                {
                    model: MovieAuthors
                },
                {
                    model: MovieFrags
                },
                {
                    model: MovieTags
                }
            ],
            order: orders,
            offset,
            limit
        })
    }

}

module.exports = new MovieDB()

