const { MovieAuthors }  = require('../db')

function MovieAuthorDB() {
    
}

MovieAuthorDB.prototype = {
    constructor: MovieAuthorDB,

    create: async function({ id = undefined, movieid, name, desc}) {
        return await MovieAuthors.create({
            id,
            movieid,
            name,
            desc
        })
    },

    deleteById: async function(id) {
        return await MovieAuthors.destroy({
            where: {
                id: id
            }
        })
    },

    deleteByMovieId: async function (movieid) {
        return await MovieAuthors.destroy({
            where: {
                movieid: movieid
            }
        })
    },

    updateById: async function(id, { movieid = undefined, name = undefined, desc = undefined }) {
        return await MovieAuthors.update({
            movieid,
            name,
            desc
        }, {
            where: {
                id: id
            }
        })
    },

    findById: async function (id) {
        return await MovieAuthors.findOne({
            where: {
                id: id
            }
        })
    },

    findByMovieid: async function (movieid) {
        return await MovieAuthors.findAll({
            where: {
                movieid: movieid
            }
        })
    }

}

module.exports = new MovieAuthorDB()

