const { MovieTags }  = require('../db')

function MovieTagDB() {
    
}

MovieTagDB.prototype = {
    constructor: MovieTagDB,

    create: async function({ id = undefined, movieid, title}) {
        return await MovieTags.create({
            id,
            movieid,
            title
        })
    },

    deleteById: async function(id) {
        return await MovieTags.destroy({
            where: {
                id: id
            }
        })
    },

    deleteByMovieId: async function (movieid) {
        return await MovieTags.destroy({
            where: {
                movieid: movieid
            }
        })
    },

    updateById: async function(id, { movieid = undefined, title = undefined }) {
        return await MovieTags.update({
            movieid,
            title
        }, {
            where: {
                id: id
            }
        })
    },

    findById: async function (id) {
        return await MovieTags.findOne({
            where: {
                id: id
            }
        })
    },

    findByMovieid: async function (movieid) {
        return await MovieTags.findAll({
            where: {
                movieid: movieid
            }
        })
    }

}

module.exports = new MovieTagDB()

