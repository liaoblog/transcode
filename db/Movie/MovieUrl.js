const { MovieUrls }  = require('../db')
const { createUUID } = require('../../utils/utils')
const { Op } = require('sequelize')
function MovieUrlDB() {
    
}

MovieUrlDB.prototype = {
    constructor: MovieUrlDB,

    ORDER_CREATEDAT_ASC: ['createdAt', 'ASC'],
    ORDER_CREATEDAT_DESC: ['createdAt', 'DESC'],

    ORDER_UPDATEDAT_ASC: ['updatedAt', 'ASC'],
    ORDER_UPDATEDAT_DESC: ['updatedAt', 'DESC'],

    create: async function(movieurl) {
        movieurl.status = 0
        return await MovieUrls.create(movieurl)
    },

    deleteById: async function(id) {
        return await MovieUrls.destroy({
            where: {
                id: id
            }
        })
    },

    deleteByMovieId: async function (movieid) {
        return await MovieUrls.destroy({
            where: {
                movieid: movieid
            }
        })
    },

    updateById: async function(id, movieurl) {
        return await MovieUrls.update(movieurl, {
            where: {
                id: id
            }
        })
    },

    updateByStatus: async function(oldstatus, movieurl) {
        return await MovieUrls.update(movieurl, {
            where: {
                status: oldstatus
            }
        })
    },

    findById: async function (id) {
        return await MovieUrls.findOne({
            where: {
                id: id
            }
        })
    },

    findByStatus: async function (status, { orders = [], offset = undefined, limit = undefined } = {}) {
        return await MovieUrls.findAll({
            where: {
                status
            },
            order: orders,
            offset,
            limit
        })
    },

    findByMovieid: async function (movieid) {
        return await MovieUrls.findAll({
            where: {
                movieid: movieid
            }
        })
    },


    getMovieNotStatus: async function (movieid, status) {
        return await MovieUrls.findAll({
            where: {
                [Op.and]: [
                    {
                        movieid: movieid
                    },
                    {
                        status: {
                            [Op.ne]: status
                        }
                    }
                ]
            },
        })
    }

}

module.exports = new MovieUrlDB()

