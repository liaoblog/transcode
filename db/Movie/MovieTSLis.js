const { MovieTSLis } = require('../db')
const { Op } = require('sequelize')


const { MovieTSLis_SYMBOL } = require('../../bean/movietslis')

function MovieTSLisDB() {}

MovieTSLisDB.prototype = {
    constructor: MovieTSLisDB,

    ORDER_CREATEDAT_ASC: ['createdAt', 'ASC'],
    ORDER_CREATEDAT_DESC: ['createdAt', 'DESC'],

    ORDER_UPDATEDAT_ASC: ['updatedAt', 'ASC'],
    ORDER_UPDATEDAT_DESC: ['updatedAt', 'DESC'],

    create: async function (movietslis) {
        return await MovieTSLis.create(movietslis)
    },

    bulkCreate: async function (movietslis) {
        return await MovieTSLis.bulkCreate(movietslis)
    },

    deleteById: async function (id) {
        return await MovieTSLis.destroy({
            where: {
                id: id
            }
        })
    },

    deleteByMovieId: async function (movieid) {
        return await MovieTSLis.destroy({
            where: {
                movieid: movieid
            }
        })
    },

    deleteByDefinitionId: async function (definitionid) {
        return await MovieTSLis.destroy({
            where: {
                definitionid: definitionid
            }
        })
    },

    updateById: async function (id, movietslis) {
        return await MovieTSLis.update(movietslis, {
            where: {
                id: id
            }
        })
    },

    updateByCryptStatus: async function (oldstatus, movietslis) {
        return await MovieTSLis.update(movietslis, {
            where: {
                cryptstatus: oldstatus
            }
        })
    },

    updateByUploadStatus: async function (oldstatus, movietslis) {
        return await MovieTSLis.update(movietslis, {
            where: {
                uploadstatus: oldstatus
            }
        })
    },

    // 获取需要上传的数据
    getUploads: async function ({ orders = [], offset = undefined, limit = undefined } = {}) {
        return await MovieTSLis.findAll({
            where: {
                [Op.and]: [
                    {
                        uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_WAIT
                    },
                    {
                        [Op.or]: [
                            {
                                cryptstatus: MovieTSLis_SYMBOL.CRYPT_STATUS
                            },
                            {
                                cryptstatus: MovieTSLis_SYMBOL.CRYPT_STATUS_SUCC
                            }
                        ]
                    }
                ]
            },
            order: orders,
            offset,
            limit
        })
    },

    findById: async function (id) {
        return await MovieTSLis.findOne({
            where: {
                id: id
            }
        })
    },

    findByCryptStatus: async function (status, { orders = [], offset = undefined, limit = undefined } = {}) {
        return await MovieTSLis.findAll({
            where: {
                cryptstatus: status
            },
            order: orders,
            offset,
            limit
        })
    },

    findByUploadStatus: async function (status, { orders = [], offset = undefined, limit = undefined } = {}) {
        return await MovieTSLis.findAll({
            where: {
                uploadstatus: status
            },
            order: orders,
            offset,
            limit
        })
    },

    findByMovieid: async function (movieid) {
        return await MovieTSLis.findAll({
            where: {
                movieid: movieid
            }
        })
    },

    findByDefinitionid: async function (definitionid) {
        return await MovieTSLis.findAll({
            where: {
                definitionid
            }
        })
    },

    findByType: async function (definitionid, type) {
        return await MovieTSLis.findAll({
            where: {
                definitionid,
                type
            }
        })
    },

    // 获取加密中上传中的数据
    getSyncData: async function (definitionid, { orders = [], offset = undefined, limit = undefined } = {}) {
        return await MovieTSLis.findAll({
            where: {
                [Op.and]: [
                    {
                        definitionid
                    },
                    {
                        [Op.or]: [
                            {
                                [Op.or]: [
                                    {
                                        cryptstatus: {
                                            [Op.eq]: MovieTSLis_SYMBOL.CRYPT_STATUS_WAIT
                                        }
                                    },
                                    {
                                        cryptstatus: {
                                            [Op.eq]: MovieTSLis_SYMBOL.CRYPT_STATUS_FAIL
                                        }
                                    }
                                ]
                            },
                            {
                                [Op.or]: [
                                    {
                                        uploadstatus: {
                                            [Op.eq]: MovieTSLis_SYMBOL.UPLOAD_STATUS_WAIT
                                        }
                                    },
                                    {
                                        uploadstatus: {
                                            [Op.eq]: MovieTSLis_SYMBOL.UPLOAD_STATUS_FAIL
                                        }
                                    },
                                    {
                                        uploadstatus: {
                                            [Op.eq]: MovieTSLis_SYMBOL.UPLOAD_STATUS_ING
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            order: orders,
            offset,
            limit

        })
    }

}

module.exports = new MovieTSLisDB()

