
const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影的url表
 */
module.exports = sequelize.define('MovieUrls', {
    
    id: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUID
    },

    movieid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    // 分辨率
    definition: {
        type: DataTypes.STRING(11),
        allowNull: false
    },

    
    url: {
        type: DataTypes.STRING,
        allowNull: true
    },

    // 转码后的文件名字（包含后缀）
    transfilename: {
        type: DataTypes.STRING(20),
        allowNull: true
    },

    // 转码后的切片文件后缀
    transtssuffix: {
        type: DataTypes.STRING(10),
        allowNull: true
    },

    // 电影转码状态
    status: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 0   // 0: 等待转码   1: 正在转码  10: 转码完成  4: 转码失败
    },

    // 备注
    remark: {
        type: DataTypes.STRING,
        allowNull: true
    }

}, {
    tableName: 'trans_movieurls'
})