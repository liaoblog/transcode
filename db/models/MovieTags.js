
const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影标签表
 */
module.exports = sequelize.define('MovieTags', {
    
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        unique: true,
        autoIncrement: true
    },

    movieid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    // 名称：如悬疑
    title: {
        type: DataTypes.STRING(20),
        allowNull: false
    }

}, {
    tableName: 'trans_movietags'
})