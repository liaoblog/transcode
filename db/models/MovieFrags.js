const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影包含的片段（截图、gif）
 */
module.exports = sequelize.define('MovieFrags', {
    
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        unique: true,
        autoIncrement: true
    },

    movieid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    // 片段url
    fragmenturl: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    tableName: 'trans_moviefrags'
})