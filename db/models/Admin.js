const { DataTypes } = require('sequelize');
const { sequelize } = require('../../dbConn');

/**
 * 后台管理员账号表
 */
module.exports = sequelize.define('Admin', {
    
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        unique: true,
        autoIncrement: true
    },

    // 用户名
    username: {
        type: DataTypes.STRING(16),
        unique: true,
        allowNull: false,
        defaultValue: 'admin'
    },

    // 密码
    password: {
        type: DataTypes.STRING(61),
        allowNull: false,
        defaultValue: 'admin'
    },

    token: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    }

}, {
    tableName: 'trans_admin'
})