const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影表
 */
module.exports = sequelize.define('Movies', {
    
    id: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUID
    },

    // 电影文件的哈希值
    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },

    // 电影封面
    poster: {
        type: DataTypes.STRING,
        allowNull: true
    },

    // 电影名字
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    //后缀
    suffix: {
        type: DataTypes.STRING(10),
        allowNull: false
    },

    // 电影大小
    size: {
        type: DataTypes.STRING,
        allowNull: true
    },

    // 电影长度
    duration: {
        type: DataTypes.STRING,
        allowNull: true
    },

    // 电影发布时间
    time: {
        type: DataTypes.DATE,
        allowNull: true
    },

    // 转码完成时间，有其中一个分辨率转码完成就算完成
    transedAt: {
        type: DataTypes.DATE,
        allowNull: true
    },

    // 电影描述
    desc: {
        type: DataTypes.STRING,
        allowNull: true
    }

}, {
    tableName: 'trans_movies',
    version: true
})