
const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影的切片表
 */
module.exports = sequelize.define('MovieTSLis', {
    
    id: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUID
    },

    movieid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    definitionid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING(20),
        allowNull: false
    },

    // 加密状态
    cryptstatus: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 0   // 0: 无需加密   1: 等待加密  10: 加密完成  4：加密失败
    },

    // 上传状态
    uploadstatus: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 0   // 0：无需上传   1：等待上传   2：上传中   10：上传完成   4：上传失败
    },

    // 备注
    remark: {
        type: DataTypes.STRING,
        allowNull: true
    },

    type: {
        type: DataTypes.STRING(10),
        allowNull: true
    }

}, {
    tableName: 'trans_movietslis'
})