const { DataTypes } = require('sequelize');
const {sequelize} = require('../../dbConn');

/**
 * 电影演员表
 */
module.exports = sequelize.define('MovieAuthors', {
    
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
        unique: true,
        autoIncrement: true
    },

    movieid: {
        type: DataTypes.STRING(64),
        primaryKey: true,
        allowNull: false
    },

    // 名字
    name: {
        type: DataTypes.STRING(20),
        allowNull: false
    },

    // 描述：如导演
    desc: {
        type: DataTypes.STRING(10),
        allowNull: false
    }

}, {
    tableName: 'trans_movieauthors'
})