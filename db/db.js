const MovieAuthors = require('./models/MovieAuthors')
const Admin = require('./models/Admin')
const MovieFrags = require('./models/MovieFrags')
const Movies = require('./models/Movies')
const MovieTags = require('./models/MovieTags')
const MovieUrls = require('./models/MovieUrls')
const MovieTSLis = require('./models/MovieTSLis')

Movies.hasMany(MovieAuthors, {foreignKey: 'movieid', sourceKey: 'id'})
MovieAuthors.belongsTo(Movies, {foreignKey: 'movieid', targetKey: 'id'})

Movies.hasMany(MovieFrags, {foreignKey: 'movieid', sourceKey: 'id'})
MovieFrags.belongsTo(Movies, {foreignKey: 'movieid', targetKey: 'id'})

Movies.hasMany(MovieTags, {foreignKey: 'movieid', sourceKey: 'id'})
MovieTags.belongsTo(Movies, {foreignKey: 'movieid', targetKey: 'id'})

Movies.hasMany(MovieUrls, {foreignKey: 'movieid', sourceKey: 'id'})
MovieUrls.belongsTo(Movies, {foreignKey: 'movieid', targetKey: 'id'})

Movies.hasMany(MovieTSLis, {foreignKey: 'movieid', sourceKey: 'id'})
MovieTSLis.belongsTo(Movies, {foreignKey: 'movieid', targetKey: 'id'})

MovieUrls.hasMany(MovieTSLis, {foreignKey: 'definitionid', sourceKey: 'id'})
MovieTSLis.belongsTo(MovieUrls, {foreignKey: 'definitionid', targetKey: 'id'})


module.exports = {
    Admin,
    MovieAuthors,
    MovieFrags,
    Movies,
    MovieTags,
    MovieUrls,
    MovieTSLis
}
