const fs = require('fs');
let files = fs.readdirSync(__dirname + '/models');
let js_files = files.filter((f)=>{
    return f.endsWith('.js');
}, files);
console.log(js_files);
for (var f of js_files) {
    console.log(`import model from file ${f}...`);
    require(__dirname + '/models/' + f).sync({
        alter: true
    });
}
