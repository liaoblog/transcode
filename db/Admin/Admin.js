const { createUUID } = require('../../utils')
const { Admin } = require('../db')
const { Op } = require("sequelize");
function AdminDB() {

}

AdminDB.prototype = {
    constructor: AdminDB,
    findByToken: async function (token) {
        return await Admin.findOne({
            attributes: ['username', 'token'],
            where: {
                token: token
            }
        })
    },

    findUser: async function (username, password) {
        return await Admin.findOne({
            attributes: ['username', 'token'],
            where: {
                username: username,
                password: password
            }
        })
    },

    findByUserName: async function (username) {
        return await Admin.findOne({
            attributes: ['username', 'token'],
            where: {
                username: username
            }
        })
    },

    updateByUserName: async function (username, { password = undefined, token = undefined }) {
        return await User.update({ password: password , token: token}, {
            where: {
                username: username
            }
        });
    }

}

module.exports = new AdminDB()

