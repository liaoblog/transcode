const ffmpeg = require ("fluent-ffmpeg");

const nodepath = require('path')

class FFMPEGB {

    /**
     * 
     * @param {*} path 输入源路径
     * @param {*} ffmpegpath ffmpeg路径
     * @param {*} outputpath 输出文件夹路径
     * @param {*} outfilename 输出文件名称
     * @param {*} outpath 输出文件全路径
     */
    constructor (path, options = {}) {

        if (options['ffmpegpath']) {
            ffmpeg.setFfmpegPath(nodepath.join(options['ffmpegpath'], 'ffmpeg.exe'))

            ffmpeg.setFfprobePath(nodepath.join(options['ffmpegpath'], 'ffprobe.exe'))
        }

        
        this.path = path  
        this.outputpath = options.output && options.output.path ? options.output.path : undefined
        this.outfilename = options.output ? options.output.name : undefined
        this.outpath = this.outputpath && this.outfilename ? this.outputpath + nodepath.sep + this.outfilename : undefined
        this.options = options
        this.outputsuffix = options.outputsuffix ? options.outputsuffix : 'ts'

        this._transend = false

        this._events = {}

        
        this._ffmpeg = ffmpeg(this.path)
        this._bindOn()
    }

    _bindOn() {
        let that = this
        this._ffmpeg
        .on('start', function(){
            if (that._events['start']) that._events['start']()
  
        })
        .on('error', function(err) {
            if (that._events['error']) that._events['error'](err)
        })
        .on('progress', function(progress) {
            if (that._events['progress']) that._events['progress'](progress)
        })
        .on('end', function() {
            if (that._events['end']) that._events['end']()
        })
    }

    on (str, callback) {

        this._events[str] = callback
        return this;
    }

    
    /**
     * 
     * 此方法默认截取为gif， 截取图片可将 t 与 r 设置为 1
     * 截取图片时， t 与 r 值必须为 1，且输出后缀不能为gif
     * 
     * @param {*} ss 截取的开始时间/秒
     * @param {*} t 截图的持续时间/秒
     * @param {*} out 输出路径
     * @param {*} r 截取帧率
     * @param {*} count 截取数量/ count为数组则为自定义截取，为数字则智能截取
     * @param {*} callback 回调函数，可取得截取的图片名称数组
     * @param {*} isFrameKey 以视频关键帧为准自动截取，否则为视频长度
     * 
     * 
     */
    async cut({ss = 0, t = 1.5, name = 'cutindex', path, r = 15, count = 1, isFrameKey = false} = {}, callback) {

        console.log('截取数量:', count)

        path = path ? path : this.outputpath

        let res = []

        if (count instanceof Array) {
            
            for (let i=0; i<count.length; i++) {

                ss = count[i]['ss']
                t = count[i]['t']
                r = count[i]['r']

                let rName = name.replace(/%d/g, i)
                t == 1 && r == 1 ? rName += '.png' : rName += '.gif'
                let filePath = nodepath.join(path, rName)

                res.push(rName)

                this._cut(filePath, { ss, t, r })
            }


        } else {
            
            let videoDuration = 0

            let frameKeys = []

            if (isFrameKey) {
                frameKeys = await this.getFrameKeyTime()

                videoDuration = frameKeys.length
            } else {
                let videoInfo = await this.getVideoInfo()
                videoDuration = Math.floor(videoInfo['format']['duration'])
            }
            
            // let videoDuration = Math.floor(videoInfo['streams'][0]['duration'])
            let resCount = 100 / (count + 1)

            if (resCount >= 1) {
                for (let i=0; i<count; i++) {

                    isFrameKey ? ss = frameKeys[Math.floor(videoDuration * (resCount * (i + 1)) / 100 - 1)] : ss = videoDuration * (resCount * (i + 1)) / 100
                
                    let rName = name.replace(/%d/g, i)
                    t == 1 && r == 1 ? rName += '.png' : rName += '.gif'
                    let filePath = nodepath.join(path, rName)

                    res.push(rName)

                    this._cut(filePath, { ss, t, r })
                }
            }
        }

        if (callback) callback(res)

        return this
    }

    async trans() {
        let that = this

        let fm = this._ffmpeg

        let options = await this._creatCommad(this.options)

        fm.videoFilters(options.vf)
        .outputOptions(options.o)

        if (this.outpath) {

            if(options.vf.length<=0 && !options.scale) {
                fm.addOptions(
                    [
                        "-c copy",
                    ]
                )
            }
            fm
            .outputOptions(
                ['-bsf:a', 'aac_adtstoasc', '-bsf:v', 'h264_mp4toannexb', '-f', 'hls', '-hls_playlist_type', 'vod', '-hls_flags', 'split_by_time', '-hls_time', '10', '-hls_segment_filename', `${that.outputpath}${nodepath.sep}${that.outfilename.split('.')[0]}%d.${that.outputsuffix}`]
            )
            fm.save(this.outpath)
        }

        return this
        
    }


    _cut(path, {ss = 0, t = 1.5, r = 15} = {}) {
        let fm = this._ffmpeg
        // let fm = ffmpeg()
        let that = this
        if (this.path) {
            fm
            .output(path)
            .outputOptions(
                ['-ss', ss, '-i', that.path, '-t', t, '-r', r, '-an']
            )
            .run()
            
        }
    }

    getVideoInfo() {
        let that = this
        return new Promise(function(res, rej) {
            that._ffmpeg.ffprobe(function(err, data) {
                if (err) rej(err)
                res(data)
            })
        })
    }

    

    getFrameKeyTime() {
        let that = this
        return new Promise(function(res, rej) {
            that._ffmpeg.ffprobe(['-v', 'quiet', '-select_streams', 'v', '-show_entries', 'frame=pkt_pts_time,pict_type'], function(err, data) {
                if (err) rej(err)
                let ress = []
                let frames = data['frame']
                frames.forEach(ele => {
                    if (ele['pict_type'] && ele['pict_type'] == 'I') {
                        ress.push(ele['pkt_pts_time'])
                    }
                });
                res(ress)
            })
        })
    }

    _pointCommad (typeObj) {


        let xStr = ``
        let yStr = ``

        let dirX = false
        let dirY = false

        if (typeObj['directionX'] && typeObj['directionX'] == 2) {
            dirX = true
        }
        if (typeObj['directionY'] && typeObj['directionY'] == 2) {
            dirY = true
        }

        if(typeObj['high']) {
            let high = typeObj['high']


            
            if(parseInt((high['start']))>=0 && parseInt(high['end']) >= 0){
                xStr += `if(gte(t, ${high['start']})* lte(t, ${high['end']}),`
                yStr += `if(gte(t, ${high['start']})* lte(t, ${high['end']}),`
            }

            high['time'] = high['time'] && high['time'] > 0 ? high['time'] : 1
            high['durtime'] = high['durtime'] && high['durtime'] > 0 ? high['durtime'] : 2

            if (high['time'] >= high['durtime']) {
                xStr += `if(lt(mod(t,${high['time']}),${high['durtime']}), `
                yStr += `if(lt(mod(t,${high['time']}),${high['durtime']}), `
            }

            xStr += `${dirX?'W-':''}W*${typeObj['x']}`
            yStr += `${dirY?'H-':''}H*${typeObj['y']}`

            if (high['xoffset'] > 0) {
                if (high['effect'] && high['effect'] == 'loop') {
                    xStr += `${dirX?'-':'+'}mod(t*(W/${high['xoffset']}), W)`
                } else {
                    xStr += `${dirX?'-':'+'}t*(W/${high['xoffset']})`
                }
            }
            if (high['yoffset'] > 0) {
                if (high['effect'] && high['effect'] == 'loop') {
                    yStr += `${dirY?'-':'+'}mod(t*(H/${high['yoffset']}), H)`
                } else {
                    yStr += `${dirY?'-':'+'}t*(H/${high['yoffset']})`
                }
            }

            if (high['time'] >= high['durtime']) {
                xStr += `, NAN)`
                yStr += `, NAN)`
            }

            if(parseInt((high['start']))>=0 && parseInt(high['end']) >= 0){
                xStr += `, NAN)`
                yStr += `, NAN)`
            }
            return [xStr, yStr]




        }
        xStr = `${dirX?'W-':''}W*${typeObj['x']}`
        yStr = `${dirY?'H-':''}H*${typeObj['y']}`

        return [xStr, yStr]

    }


    async _creatCommad (options) {

        


        let iCom = []
        let oCom = []
        let vfCom = []

        if (options['filterComp']) {

            // if (Object.keys(options['filterComp']).length > 0) iCom.push('-filter_complex')

            if (options['filterComp']['overlay'] && options['filterComp']['overlay'].length > 0) {
                let str = ''
                let len = options['filterComp']['overlay'].length

                let videoInfo = await this.getVideoInfo()
                const videoW = videoInfo['streams'][0]['width']
                const videoH = videoInfo['streams'][0]['height']

                //输入图片流
                for (let i = 0; i < len; i++) {
                    
                    if (options['filterComp']['overlay'][i]['img']) {
                        str += `movie=${options['filterComp']['overlay'][i]['img']['path']}`
                        if (options['filterComp']['overlay'][i]['img']['w'] && options['filterComp']['overlay'][i]['img']['h']) {
                            str += `,scale=${videoW * options['filterComp']['overlay'][i]['img']['w']}:${videoH * options['filterComp']['overlay'][i]['img']['h']}`
                        }
                        if(options['filterComp']['overlay'][i]['img']['opacity']) {
                            str += `,lut=a=val*${options['filterComp']['overlay'][i]['img']['opacity'] / 10}`
                        }
                        str += `[v${i}];`
                    }


                }

                str += '[in]'

                
                for (let i = 0; i < len; i++) {
                    let overlayObj = options['filterComp']['overlay'][i]

                    if ( i != 0) str += `[i${i-1}]`
                    //图片水印属性
                    if (options['filterComp']['overlay'][i]['img']) {
                        str += `[v${i}]overlay=x='${this._pointCommad(overlayObj)[0]}':y='${this._pointCommad(overlayObj)[1]}'`
                    }

                    if ( i != len -1 ) {
                        if (Object.keys(options['filterComp']['overlay'][i]).indexOf('img') != -1 && Object.keys(options['filterComp']['overlay'][i + 1]).indexOf('img') != -1) {
                            str += `[i${i}],`
                        }
                    }
                    
                    //文字水印属性
                    if (options['filterComp']['overlay'][i]['text']) {
                        let textObjCont = options['filterComp']['overlay'][i]['text']
                        let textObj = options['filterComp']['overlay'][i]

                        if ( i != 0) str += `;[i${i-1}]`
                        str += `drawtext=text=\'${textObjCont['cont']}\':x='${this._pointCommad(textObj)[0]}':y='${this._pointCommad(textObj)[1]}'`
                        if(textObjCont['fontsize']) {
                            str += `:fontsize=12*${textObjCont['fontsize']}`
                        }
                        if(textObjCont['fontcolor']) {
                            str += `:fontcolor=${textObjCont['fontcolor']}`
                        } else {
                            str += `:fontcolor=black`
                        }
                        if(textObjCont['opacity']) {
                            str += `@${textObjCont['opacity'] / 10}`
                        }
                        if(textObjCont['shadowx']) {
                            str += `:shadowx=${textObjCont['shadowx']}`
                        }
                        if(textObjCont['shadowy']) {
                            str += `:shadowy=${textObjCont['shadowy']}`
                        }
                        if(textObjCont['fontfile']) {
                            str += `:fontfile=${textObjCont['fontfile']}`
                        }
                        
                        
                    }

                }
                vfCom.push(str)
            }

        }
        // iCom.push('-hwaccel')
        // iCom.push('cuvid')
        // iCom.push('-c:v')
        // iCom.push('h264_cuvid')

        if (options['scale']) {
            console.log(options['scale'])
            oCom.push('-s')
            oCom.push(options['scale'])

        }
        // oCom.push('-c:v')
        // oCom.push('h264_nvenc')
        
        if (options['output'] &&　options['output']['bitrate']) {
            oCom.push('-b')
            oCom.push(options['output']['bitrate'])
        }

        

        

        return {
            i: iCom,
            o: oCom,
            vf: vfCom
        }
    }
}



module.exports =  FFMPEGB