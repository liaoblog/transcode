

function MovieTSLis({ id = undefined, movieid = undefined, definitionid = undefined, name = undefined, cryptstatus = undefined, uploadstatus = undefined, remark = undefined, type = undefined } = {}) {
    
    this.id = id
    this.movieid = movieid
    this.definitionid = definitionid
    this.name = name
    this.cryptstatus = cryptstatus
    this.uploadstatus = uploadstatus
    this.remark = remark
    this.type = type

}
MovieTSLis.prototype = {
    constructor: MovieTSLis

}

module.exports = {
    MovieTSLisBean: MovieTSLis,
    MovieTSLis_SYMBOL: {
        CRYPT_STATUS: 0, // 无需加密
        CRYPT_STATUS_WAIT: 1, // 等待加密
        CRYPT_STATUS_SUCC: 10, // 加密成功
        CRYPT_STATUS_FAIL: 4, // 加密失败.

        UPLOAD_STATUS: 0, // 无需上传
        UPLOAD_STATUS_WAIT: 1, // 等待上传
        UPLOAD_STATUS_ING: 2, // 上传中
        UPLOAD_STATUS_SUCC: 10, // 上传成功
        UPLOAD_STATUS_FAIL: 4, // 上传失败

        FILETYPE_IMAGE: 'image',
        FILETYPE_TS: 'ts',
        FILETYPE_M3U8: 'm3u8'
    }
}