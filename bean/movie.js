

function Movie({ id = undefined, hash = undefined, poster = undefined, title = undefined, suffix = undefined, size = undefined, time = undefined, duration = undefined, desc = undefined, transedAt = undefined } = {}) {

    this.id = id
    this.hash = hash
    this.poster = poster
    this.title = title
    this.suffix = suffix
    this.size = size
    this.time = time
    this.duration = duration
    this.desc = desc
    this.transedAt = transedAt

}
Movie.prototype = {
    constructor: Movie
}

module.exports = {
    MovieBean: Movie
}