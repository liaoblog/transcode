
function MovieUrl({ id = undefined, movieid = undefined, definition = undefined, url = undefined, transfilename = undefined, transtssuffix = undefined, status = undefined, remark = undefined } = {}) {
    this.id = id
    this.movieid = movieid
    this.definition = definition
    this.url = url
    this.transfilename = transfilename
    this.transtssuffix = transtssuffix
    this.status = status
    this.remark = remark
}
MovieUrl.prototype = {
    constructor: MovieUrl
}

module.exports = {
    MovieUrlBean: MovieUrl,
    MovieUrl_SYMBOL: {
        TRANS_STATUS: 0, // 等待转码
        TRANS_STATUS_ING: 1, // 转码中,
        TRANS_STATUS_SUCC: 10, // 转码成功
        TRANS_STATUS_FAIL: 4, // 转码失败
    }
}