const minio = require('./minio')
const ali = require('./ali')
const tencent = require('./tencent')

const { transdb } = require('../lowdb/index')

function OSS() {

    this.ossIntances = []

    this.init()

}
OSS.prototype = {
    constructor: OSS,

    init: function () {

        let osses = transdb.get('osses').value()

        osses.forEach(v => {
            if (v.type == minio.OSS_TYPE) {
                this.ossIntances.push({
                    type: minio.OSS_TYPE,
                    ins: new minio.MinIo({
                        endPoint: v.endPoint,
                        port: v.port,
                        accessKey: v.accessKey,
                        secretKey: v.secretKey,
                        useSSL: v.useSSL,
                        bucket: v.bucket
                    })
                })
            }

            if (v.type == ali.OSS_TYPE) {
                this.ossIntances.push({
                    type: ali.OSS_TYPE,
                    ins: new ali.Ali({
                        endpoint: v.endPoint,
                        bucket: v.bucket,
                        accessKeyId: v.accessKey,
                        accessKeySecret: v.secretKey
                    })
                })
            }

            if (v.type == tencent.OSS_TYPE) {
                this.ossIntances.push({
                    type: tencent.OSS_TYPE,
                    ins: new tencent.Tencent({
                        Region: v.endPoint,
                        Bucket: v.bucket,
                        SecretId: v.accessKey,
                        SecretKey: v.secretKey
                    })
                })
            }
        })
    },

    getInstances: function () {
        return this.ossIntances
    },


    upload: async function ({
        fileName,
        filePath,
        index,
        succCallback,
        failCallback,
        startCallback
    }) {
        if (startCallback) await startCallback()
        this.ossIntances[index].ins.upload(fileName, filePath, succCallback || function () { }, failCallback || function () { })
    }

}

module.exports = new OSS()