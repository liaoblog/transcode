const tencent = require('cos-nodejs-sdk-v5')

const { transdb } = require('../lowdb/index')

const fs = require('fs')

function Tencent( { SecretId, SecretKey, Region, Bucket }) {

    this.Region = Region
    this.Bucket = Bucket

    this.instance = new tencent({
        SecretId,
        SecretKey,
        ChunkParallelLimit: transdb.get('uploadCount').value()
    })

}
const OSS_TYPE = 'tencent'
Tencent.prototype = {
    constructor: Tencent,
    getTencentIntance: function () {
      return this.instance  
    },

    upload: function (fileName, filePath, succCallback, failCallback) {

        let self = this

        this.instance.putObject({
            Bucket: self.Bucket,
            Region: self.Region,
            Key: fileName,
            StorageClass: 'STANDARD',
            Body: fs.createReadStream(filePath)
        }, function (err, data) {
            if (err) {
                failCallback(err)
            } else {
                succCallback({
                    type: OSS_TYPE,
                    result: data
                })
            }
        })

    }

}

module.exports = {
    Tencent: Tencent,
    OSS_TYPE
}