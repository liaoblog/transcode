const ali = require('ali-oss')

function Ali( { endpoint, bucket, accessKeyId, accessKeySecret }) {

    this.instance = new ali({
        endpoint,
        bucket,
        accessKeyId,
        accessKeySecret
    })

}
const OSS_TYPE = 'ali'
Ali.prototype = {
    constructor: Ali,
    TYPE: 'ali',
    getAliIntance: function () {
      return this.instance  
    },

    upload: function (fileName, filePath, succCallback, failCallback) {
        let self = this
        this.instance.put(fileName, filePath)
        .then(result => {
            succCallback({
                type: OSS_TYPE,
                result
            })
        })
        .catch(e => {
            failCallback(e)
        })

    }

}

module.exports = {
    Ali,
    OSS_TYPE
}