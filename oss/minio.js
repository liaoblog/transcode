const Minio = require('minio')

function MinIo({ endPoint = '127.0.0.1', port = 9000, useSSL = false, accessKey = 'minioadmin', secretKey = 'minioadmin', bucket }) {

    this.bucket = bucket

    this.instance = new Minio.Client({
        endPoint,
        port,
        useSSL,
        accessKey,
        secretKey
    })

}

const OSS_TYPE = 'minio'

MinIo.prototype = {
    constructor: MinIo,
    DEFAULT_META: {
        'Content-Type': 'application/octet-stream'
    },

    getMinIoIntance: function () {
        return this.instance
    },

    // 'D:\\nodejs\\transcodeServer\\transcode\\videos\\8ef51880bae011eb9911b1971633be6f\\8f1b8c40bae011eb9911b1971633be6f\\index0.ts'
    upload: function (fileName, filePath, succCallback, failCallback) {
        let self = this
        this.instance.fPutObject(this.bucket, fileName, filePath, this.DEFAULT_META, function (err, etag) {
            if (err) failCallback(err)
            else {
                succCallback({
                    type: OSS_TYPE,
                    result: etag
                })
            }
        })

    }

}

module.exports = {
    MinIo: MinIo,
    OSS_TYPE: OSS_TYPE
}