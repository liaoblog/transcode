const chokidar = require('chokidar');
const nodepath = require('path');
const Movie = require('./services/Movie');
const { transdb, transingdb } = require('./lowdb/index')
const suffixs = ['mp4', 'flv']


if (!process.env.NODE_APP_INSTANCE || process.env.NODE_APP_INSTANCE === '0') {
    
    chokidar.watch(nodepath.join(__dirname, 'movies')).on('all', async (event, path) => {
    
        if (event == 'change') {
            // 获取文件后缀
            let fileSuffix = path.split('.')
            fileSuffix = fileSuffix[fileSuffix.length - 1]

            if (suffixs.indexOf(fileSuffix) != -1) {

                // 获取文件名（包含后缀）
                let fullFileName = path.split(nodepath.sep)
                fullFileName = fullFileName[fullFileName.length - 1]

                await Movie.addMovie(fullFileName)

                // 
                Movie.trans()


            }


        }
    })
}

