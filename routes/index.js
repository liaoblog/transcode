var express = require('express');
var router = express.Router();
const Movie = require('../services/Movie')

const oss = require('../oss/index');
const cms = require('../cms');

/* GET home page. */
router.get('/',  function(req, res, next) {


  // let mm = oss.getInstances()
  // mm = mm[0]
  // mm.ins.getMinIoIntance().presignedGetObject('test', 'd115ff70bbfa11eb9c1029636d24f9df/d12d5800bbfa11eb9c1029636d24f9df', 24*60*60, function(err, presignedUrl) {
  //   if (err) return console.log(err)
  //   console.log(presignedUrl)
  // })

  cms.sync({
    videoName: '测试',
    collName: '第3集',
    url: 'https://www.bilibili.com/video/BV1cZ4y1c7sv',
    index: 3
  })

  // oss.upload({
  //   // bucketName: 'test',
  //   fileName: 'aaa/index110.ts',
  //   filePath: 'D:\\nodejs\\transcode\\index0.ts',
  //   index: 0,
  //   succCallback: function (result) {
  //     console.log(result)
  //   },
  //   failCallback: function (err) {
  //     console.log(err.message)
  //   }
  // })
  res.render('index', { title: 'Express' });
});

router.ws('/ws', function(ws, req) {
  ws.on('message', function(msg){
    console.log(msg)
  })
})

module.exports = router;
