var Sequelize = require('sequelize');



/**
 * host: 数据库地址
 * user: 数据库用户名
 * password: 数据库密码
 * database： 数据库名
 * port: 数据库端口
 */
var sqlConfig = {
    host: "localhost",
    user: "root",
    password: "root",
    database: "trans_test",
    port: 3306
};






































var sequelize = new Sequelize(sqlConfig.database, sqlConfig.user, sqlConfig.password, {
    host: sqlConfig.host,
    dialect: 'mysql',
    port: sqlConfig.port,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    timezone: '+08:00',
    dialectOptions: {
        dateStrings: true,
        typeCast: true
    },
    logging: false
})


module.exports = {
    sequelize: sequelize
}