const path = require('path')
const utils = require('../utils/utils')

const Movie = require('../db/Movie/Movie')
const MovieUrl = require('../db/Movie/MovieUrl')
const MovieTSLis = require('../db/Movie/MovieTSLis')

const ffmpegb = require('../ffmpegb')
const { transdb } = require('../lowdb/index')

const { MovieBean } = require('../bean/movie')
const { MovieUrlBean, MovieUrl_SYMBOL } = require('../bean/movieurl')
const { MovieTSLis_SYMBOL } = require('../bean/movietslis')

const MovieTrans = require('./MovieTrans')
const MovieCrypt = require('./MovieCrypt')
const MovieUpload = require('./MovieUpload')

function MovieServices() {

}

MovieServices.prototype = {
  constructor: MovieServices,

  addMovie: async function (fileName) {

    let fullFileName = fileName
    fileName = fullFileName.split('.')
    let fileSuffix = fileName[fileName.length - 1]
    fileName.pop()
    fileName = fileName.join('')

    let fileNameMd5 = utils.getStrMd5(fileName)

    let filePath = path.join(utils.getSourceSavePath(), fullFileName)
    let movieid = utils.createUUID()
    let videoInfo = await new ffmpegb(filePath, {
      ffmpegpath: 'F:\\ffmpeg-2020-11-18\\bin'
    }).getVideoInfo()

    let videoInfoStreams = videoInfo['streams']
    let videoInfoFormat = videoInfo['format']
    let videoSize = videoInfoFormat['size']
    let videoDuration = videoInfoFormat['duration']

    await Movie.create(new MovieBean({
      id: movieid,
      hash: fileNameMd5,
      title: fileName,
      suffix: fileSuffix,
      size: videoSize,
      duration: videoDuration
    }))

    let videoH = videoInfoStreams[0]['height']

    let resoutions = utils.sortDefinition()
    let resoutionsFilter = resoutions.filter((v, i) => {
      if (videoH >= parseInt(v.title)) return v
    })
    if (resoutionsFilter.length <= 0) resoutionsFilter = resoutions

    let resoutionsLen = resoutionsFilter.length
    for (let i = 0; i < resoutionsLen; i++) {
      let v = resoutionsFilter[i]
      let resoutionsId = utils.createUUID()

      await MovieUrl.create(new MovieUrlBean({
        id: resoutionsId,
        movieid: movieid,
        definition: v.desc
      }))
    }



  },

  resetMovieStatus: async function (value) {
    let urlRes = await MovieUrl.findByStatus(value)
    if (urlRes.length > 0) {
      await MovieUrl.updateByStatus(value, {
        status: MovieUrl_SYMBOL.TRANS_STATUS
      })
    }

  },

  resetMovieTSStatus: async function (value) {
    let tsRes = await MovieTSLis.findByUploadStatus(value)
    if (tsRes.length > 0) {
      await MovieTSLis.updateByUploadStatus(value, {
        uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_WAIT
      })
    }
  },

  trans: function () {

    MovieTrans.run()

  },


  tsCrypt: function () {

    MovieCrypt.run()

  },

  tsUpload: function () {

    MovieUpload.run()

  }





}

module.exports = new MovieServices()