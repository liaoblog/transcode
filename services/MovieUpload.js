const path = require('path')
const fs = require('fs')
const utils = require('../utils/utils')

const MovieTSLis = require('../db/Movie/MovieTSLis')


const { transdb } = require('../lowdb/index')


const { MovieTSLisBean, MovieTSLis_SYMBOL } = require('../bean/movietslis')

const oss = require('../oss/index')

const { uploadQueue, syncQueue, deleteMovieFileQueue } = require('../utils/queue')
const MovieUrl = require('../db/Movie/MovieUrl')
const { MovieUrlBean } = require('../bean/movieurl')
const cms = require('../cms')
const Movie = require('../db/Movie/Movie')
const minio = require('../oss/minio')

function Upload() {


}

Upload.prototype = {
    constructor: Upload,

    run: function () {
        let cryptTs = transdb.get('cryptTs').value()
        let uploadTs = transdb.get('uploadTs').value()
        if ((cryptTs != 1 && uploadTs == 1) || (cryptTs == 1 && uploadTs == 1)) {
            let uploadCount = transdb.get('uploadCount').value()

            let len = uploadCount - uploadQueue.queues.length
            if (len > 0) {
                for (let i = 0; i < len; i++) {
                    uploadQueue.exec(this.runOne.bind(this)) // 添加到上传队列
                }
            }
        }
    },

    runOne: function (movietsid) {
        return new Promise(async (resolve, reject) => {
            this._upload(movietsid)
                .then(() => {
                    resolve()
                })
        })
    },

    _upload: function (movietsid) {

        let self = this


        return new Promise(async (resolve, reject) => {


            let tses = []
            if (movietsid) {
                tses[0] = await MovieTSLis.findById(movietsid)
            } else {
                tses = await MovieTSLis.getUploads({
                    limit: 1,
                    orders: [
                        MovieTSLis.ORDER_CREATEDAT_ASC
                    ]
                })
            }

            if (tses.length) {

                tses = tses[0]
                let filePath = path.join(utils.getTsSavePath(tses.movieid, tses.definitionid), tses.name)
                if (fs.existsSync(filePath)) {

                    let osses = transdb.get('osses').value()

                    let index = 0
                    osses.forEach((v, i) => {
                        if (v.isuse) {
                            index = i
                            return
                        }
                    })
                    oss.upload({
                        fileName: `${tses.movieid}/${tses.definitionid}/${tses.name}`,
                        filePath: filePath,
                        index: index,
                        succCallback: async function () {
                            await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                                uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_SUCC
                            }))
                            let movieUrlRes = await MovieUrl.findById(tses.definitionid)
                            if (movieUrlRes) {
                                if (tses.name == movieUrlRes.transfilename) {
                                    await MovieUrl.updateById(tses.definitionid, new MovieUrlBean({
                                        url: utils.packMovieUrl(osses[index])
                                    }))

                                }
                            }


                            if (transdb.get('syncmovie').value()) {
                                // 同步信息至cms
                                let syncData = await MovieTSLis.getSyncData(tses.definitionid)
                                if (syncData.length <= 0) {
                                    let movieRes = await Movie.findById(tses.movieid)
                                    let names = movieRes.title.split('$$$')
                                    let videoName = names[0]
                                    let collName = names[1]
                                    let index = names[2]

                                    // 构造url
                                    let url = JSON.parse(movieUrlRes.url)
                                    url.movieid = movieUrlRes.movieid
                                    url.id = movieUrlRes.id
                                    url.transfilename = movieUrlRes.transfilename
                                    let urlobj = { movieurl: utils.packSyncUrl(url) }

                                    let definition = movieUrlRes.definition
                                    if (!collName || !index) {
                                        let definitions = utils.sortDefinition(true)
                                        definitions.forEach((v, i) => {
                                            if (v.desc == definition) {
                                                index = (i + 1)
                                                collName = v.chtitle
                                                return
                                            }
                                        })
                                    } else if (collName && index) {// 单个视频多集多个分辨率只同步最高的分辨率
                                        let r = await MovieUrl.findByMovieid(tses.movieid)
                                        r = r.sort(function (x, y) {
                                            x = parseInt(x.definition.split('x')[1])
                                            y = parseInt(y.definition.split('x')[1])
                                            if (x > y) {
                                                return -1
                                            } else if (x < y) {
                                                return 1
                                            }
                                            return 0
                                        })
                                        if (definition != r[0].definition) {
                                            return
                                        }
                                    }


                                    let screenhost = []
                                    let imgRes = await MovieTSLis.findByType(tses.definitionid, MovieTSLis_SYMBOL.FILETYPE_IMAGE)
                                    imgRes.forEach(v => {
                                        url.transfilename = v.name
                                        screenhost.push(utils.packSyncUrl(url))
                                    })
                                    if (screenhost.length > 0) {
                                        urlobj.screenhost = screenhost.join('#')
                                    }

                                    cms.sync({
                                        videoName,
                                        collName,
                                        index,
                                        url: urlobj,
                                        succCallback: async function (data) {

                                            syncQueue.exec(function () {
                                                return new Promise(async (resolve, reject) => {
                                                    let movieRes = await MovieUrl.findById(movieUrlRes.id)
                                                    let remark = {}
                                                    if (movieRes.remark && movieRes.remark.length > 0) remark = JSON.parse(movieRes.remark)
                                                    remark[data.id] = data
                                                    await MovieUrl.updateById(movieUrlRes.id, new MovieUrlBean({
                                                        remark: JSON.stringify(remark)
                                                    }))
                                                    resolve()
                                                })
                                            })

                                        },
                                        failCallback: async function (data) {
                                            syncQueue.exec(function () {
                                                return new Promise(async (resolve, reject) => {
                                                    let movieRes = await MovieUrl.findById(movieUrlRes.id)
                                                    let remark = {}
                                                    if (movieRes.remark && movieRes.remark.length > 0) remark = JSON.parse(movieRes.remark)
                                                    remark[data.id] = data
                                                    await MovieUrl.updateById(movieUrlRes.id, new MovieUrlBean({
                                                        remark: JSON.stringify(remark)
                                                    }))
                                                    resolve()
                                                })
                                            })
                                        }
                                    })
                                }
                            }


                            deleteMovieFileQueue.exec(function () {
                                return new Promise((resolve) => {
                                    // 删除源文件
                                    let isDeleted = transdb.get('uploadDeleted').value()
                                    if (isDeleted) {
                                        if (fs.existsSync(filePath)) fs.unlinkSync(filePath)

                                        let definitionDir = path.join(filePath, '..')
                                        // utils.deleteNullDir(definitionDir, transdb.get('tsSavePath').value())
                                        let files = fs.readdirSync(definitionDir)
                                        if (files <= 0) {
                                            fs.rmdirSync(definitionDir)
                                            definitionDir = path.join(definitionDir, '..')
                                            let files = fs.readdirSync(definitionDir)
                                            if (files <= 0) {
                                                fs.rmdirSync(definitionDir)
                                            }
                                            
                                        }

                                    }

                                    resolve()
                                })
                            })

                        },
                        failCallback: async function (err) {
                            await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                                uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_FAIL,
                                remark: `upload: ${err.message}`
                            }))
                        },
                        startCallback: async function () {
                            await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                                uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_ING
                            }))
                            resolve()
                        }
                    })

                } else {

                    await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                        uploadstatus: MovieTSLis_SYMBOL.UPLOAD_STATUS_FAIL,
                        remark: 'upload: file not found'
                    }))
                    resolve()

                }

                self.run()


            } else {
                resolve()
            }

        })



    }



}

module.exports = new Upload()