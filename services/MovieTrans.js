const path = require('path')
const fs = require('fs')
const utils = require('../utils/utils')

const Movie = require('../db/Movie/Movie')
const MovieUrl = require('../db/Movie/MovieUrl')
const MovieTSLis = require('../db/Movie/MovieTSLis')

const ffmpegb = require('../ffmpegb')
const { transdb, transingdb } = require('../lowdb/index')

const { MovieUrlBean, MovieUrl_SYMBOL } = require('../bean/movieurl')
const { MovieTSLisBean, MovieTSLis_SYMBOL } = require('../bean/movietslis')

const { transQueue, deleteMovieFileQueue } = require('../utils/queue')

const MovieCrypt = require('./MovieCrypt')
const MovieUpload = require('./MovieUpload')

function MovieTrans() { }

MovieTrans.prototype = {
    constructor: MovieTrans,

    // 添加到转码队列等待执行
    run: function () {
        if (transdb.get('transSwitch').value() == 1) {
            // 设置的转码数量
            let transCount = transdb.get('transCount').value()
            let len = transCount - transQueue.queues.length
            for (let i = 0; i < len; i++) {

                transQueue.exec(this.runOne.bind(this))

            }
        }
    },

    // 马上执行一次
    runOne: function (movieurlid) {
        return new Promise(async (resolve, reject) => {
            this._trans(movieurlid)
                .then(() => {
                    resolve()
                })
        })
    },

    cutImg: function (sourceFilePath, transConfig) {

        return new Promise((resolve, reject) => {
            //  截取缩略图/动图
            let cutOption = transdb.get('cutImg').value()
            if (cutOption.onoff) {
                if (cutOption.type == cutOption.TYPES.CUT_TYPE_CUSTOM && (cutOption.data instanceof Array && cutOption.data.length > 0)) {
                    new ffmpegb(sourceFilePath, transConfig).on('end', function () {
                        resolve()
                    }).cut({
                        count: cutOption.data,
                        name: `${cutOption.filename}%d`
                    })

                } else if (cutOption.type == cutOption.TYPES.CUT_TYPE_DEFAULT && (cutOption.data instanceof Object && !Array.isArray(cutOption.data))) {
                    if (cutOption.fileType == cutOption.TYPES.FILE_TYPE_IMG) { cutOption.data.t = 1; cutOption.data.r = 1 }
                    new ffmpegb(sourceFilePath, transConfig).on('end', function () {
                        resolve()
                    }).cut({
                        t: cutOption.data.t,
                        r: cutOption.data.r,
                        count: cutOption.data.count,
                        name: `${cutOption.filename}%d`,
                        isFrameKey: cutOption.isFrameKey
                    })

                }


            } else {
                reject()
            }
        })
    },

    _addTSToDb: function (movieid, definitionid) {
        return new Promise((resolve, reject) => {
            let cryptTs = transdb.get('cryptTs').value()
            let uploadTs = transdb.get('uploadTs').value()
            // 获取转码后所有切片文件并添加至数据库
            fs.readdir(utils.getTsSavePath(movieid, definitionid), async function (err, files) {
                if (!err) {
                    let res = await MovieUrl.findById(definitionid)
                    if (res) {
                        let filename = res.transfilename
                        let fileObjs = []

                        files.forEach(function (v, i) {

                            let obj = {
                                id: utils.createUUID(),
                                movieid: movieid,
                                definitionid: definitionid,
                                name: v,
                                cryptstatus: cryptTs ? MovieTSLis_SYMBOL.CRYPT_STATUS_WAIT : MovieTSLis_SYMBOL.CRYPT_STATUS,
                                uploadstatus: uploadTs ? MovieTSLis_SYMBOL.UPLOAD_STATUS_WAIT : MovieTSLis_SYMBOL.UPLOAD_STATUS,
                                type: MovieTSLis_SYMBOL.FILETYPE_TS
                            }

                            if (v.indexOf(transdb.get('cutImg').value().filename) >= 0) {
                                obj.cryptstatus = MovieTSLis_SYMBOL.CRYPT_STATUS
                                obj.type = MovieTSLis_SYMBOL.FILETYPE_IMAGE

                            } else if (v == filename) {
                                obj.cryptstatus = MovieTSLis_SYMBOL.CRYPT_STATUS
                                obj.type = MovieTSLis_SYMBOL.FILETYPE_M3U8
                            }

                            fileObjs.push(new MovieTSLisBean(obj))

                        })

                        MovieTSLis.bulkCreate(fileObjs)

                        resolve()

                    }
                } else {
                    reject()
                }
            })
        })
    },

    _trans: function (movieurlid) {

        let self = this

        return new Promise(async (resolve, reject) => {

            let urls = []
            if (movieurlid) {
                urls[0] = await MovieUrl.findById(movieurlid)
            } else {
                urls = await MovieUrl.findByStatus(MovieUrl_SYMBOL.TRANS_STATUS, {
                    limit: 1,
                    orders: [
                        MovieUrl.ORDER_CREATEDAT_ASC
                    ]
                })
            }

            if (urls.length) {

                urls = urls[0]

                // 转码配置(水印，跑马灯等)
                let transConfig = transdb.get('transFilter').value()

                let movie = await Movie.findById(urls.movieid)

                transConfig.scale = urls.definition
                transConfig.output = {}

                let outputpath = utils.getTsSavePath(urls.movieid, urls.id)
                // 创建切片文件夹
                if (!fs.existsSync(outputpath)) utils.mkdirs(outputpath)

                transConfig.output.path = outputpath
                transConfig.output.name = transdb.get('transfilename').value()
                transConfig.output.outputsuffix = transdb.get('transtssuffix').value()
                transConfig.ffmpegpath = 'F:\\ffmpeg-2020-11-18\\bin'
                let sourceFilePath = path.join(utils.getSourceSavePath(), `${movie.title}.${movie.suffix}`)

                let taskPromise = null

                new ffmpegb(sourceFilePath, transConfig)
                    .on('start', async function () {

                        // 添加当前转码的movie到内存, 以便获取转码进度
                        transingdb.get('data').push({
                            moviename: movie.title,
                            movieid: movie.id,
                            definitionid: urls.id,
                            prog: 0
                        }).write()

                        // 更新转码状态
                        await MovieUrl.updateById(urls.id, new MovieUrlBean({
                            status: MovieUrl_SYMBOL.TRANS_STATUS_ING,
                            transfilename: transdb.get('transfilename').value(),
                            transtssuffix: transdb.get('transtssuffix').value()
                        }))

                        // 通知任务队列: 执行完毕
                        resolve()


                        taskPromise = self.cutImg(sourceFilePath, transConfig)


                    })
                    .on('end', async function () {

                        await MovieUrl.updateById(urls.id, new MovieUrlBean({
                            status: MovieUrl_SYMBOL.TRANS_STATUS_SUCC
                        }))

                        transingdb.get('data').remove({
                            definitionid: urls.id
                        }).write()


                        deleteMovieFileQueue.exec(function () {
                           
                            return new Promise(async (resolve) => {
                                // 删除源文件
                                
                                let movieRes = await MovieUrl.getMovieNotStatus(movie.id, MovieUrl_SYMBOL.TRANS_STATUS_SUCC)
                                if (movieRes && movieRes.length <= 0) {
                                    let isDeleted = transdb.get('transDeleted').value()
                                    if (isDeleted) {
                                        // 等待一会, 等待源文件被释放
                                        setTimeout(function(){
                                            if (fs.existsSync(sourceFilePath)) fs.unlinkSync(sourceFilePath)
                                            resolve()
                                        }, 2000)
                                        
                                    } else {
                                        resolve()
                                    }
                                } else {
                                    resolve()
                                }
                            })
                        })



                        self.run()

                        taskPromise.finally(() => {
                            return self._addTSToDb(movie.id, urls.id)
                        }).finally(() => {
                            MovieCrypt.run()
                            MovieUpload.run()
                        })


                        if (transdb.get('uploadTs').value() != 1) {
                            await MovieUrl.updateById(urls.id, new MovieUrlBean({
                                url: utils.packMovieUrl()
                            }))
                        }



                    })
                    .on('error', async function (err) {
                        await MovieUrl.updateById(urls.id, new MovieUrlBean({
                            remark: err.message,
                            status: MovieUrl_SYMBOL.TRANS_STATUS_FAIL
                        }))
                        transingdb.get('data').remove({
                            definitionid: urls.id
                        }).write()
                        self.run()
                    })
                    .on('progress', function (progress) {

                        transingdb.get('data').find({
                            definitionid: urls.id,
                        })
                            .assign(
                                transingdb.get('data').find({
                                    definitionid: urls.id,
                                }).value().prog = progress.percent
                            )
                            .write()
                        console.log("🚀 ~ file: Movie.js ~ line 181 ~ transingdb.get('data').value()", transingdb.get('data').value())
                    })
                    .trans()


            } else {

                resolve()

            }



        })
    }



}

module.exports = new MovieTrans()