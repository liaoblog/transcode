const path = require('path')
const fs = require('fs')
const utils = require('../utils/utils')

const MovieTSLis = require('../db/Movie/MovieTSLis')

const { transdb } = require('../lowdb/index')


const { MovieTSLisBean, MovieTSLis_SYMBOL } = require('../bean/movietslis')

const tsCrypt = require('../utils/TSCrypt')

const { cryptQueue } = require('../utils/queue')

const MovieUpload = require('./MovieUpload')

function Crypt() {


}

Crypt.prototype = {
    constructor: Crypt,

    run: function () {
        if (transdb.get('cryptTs').value() == 1) {
            // 设置的同时加密数量
            let cryptCount = transdb.get('cryptCount').value()

            let len = cryptCount - cryptQueue.queues.length
            if (len > 0) {
                for (let i = 0; i < len; i++) {
                    cryptQueue.exec(this.runOne.bind(this))

                }
            }

        }
    },

    runOne: function (movietsid) {
        return new Promise(async (resolve, reject) => {
            this._crypt(movietsid)
            .then(() => {
                resolve()
            })
        })
    },

    _crypt: function (movietsid) {

        let self = this


        return new Promise(async (resolve, resject) => {

            let tses = []
            if (movietsid) {
                tses[0] = await MovieTSLis.findById(movietsid)
            } else {
                tses = await MovieTSLis.findByCryptStatus(MovieTSLis_SYMBOL.CRYPT_STATUS_WAIT, {
                    limit: 1,
                    orders: [
                        MovieTSLis.ORDER_CREATEDAT_ASC
                    ]
                })
            }

            if (tses.length) {

                tses = tses[0]
                
                let filePath = path.join(utils.getTsSavePath(tses.movieid, tses.definitionid) , tses.name)

                if (fs.existsSync(filePath)) {

                    tsCrypt.encryptV1(filePath, filePath, async function (err) {
                        if (err) {
                            await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                                cryptstatus: MovieTSLis_SYMBOL.CRYPT_STATUS_FAIL,
                                remark: `encrypt: ${err.message}`
                            }))
                            resolve()
                        } else {

                            await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                                cryptstatus: MovieTSLis_SYMBOL.CRYPT_STATUS_SUCC,
                            }))
                            resolve()
                            if (tses.uploadstatus == MovieTSLis_SYMBOL.UPLOAD_STATUS_WAIT) MovieUpload.run()

                        }


                    })

                } else {

                    await MovieTSLis.updateById(tses.id, new MovieTSLisBean({
                        cryptstatus: MovieTSLis_SYMBOL.CRYPT_STATUS_FAIL,
                        remark: 'encrypt: file not found'
                    }))

                    resolve()
                }

                self.run()


            } else {
                resolve()
            }


        })




    }



}

module.exports = new Crypt()