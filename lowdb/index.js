const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const Memory = require('lowdb/adapters/Memory')

const adapter = new FileSync(require('path').join(__dirname, '../', 'transConfig.json'));

const transdb = low(adapter);

const transingdb = low(new Memory())


transdb.defaults({

    siteurl: 'http://127.0.0.1', // 本站绑定域名, 包含http

    transSwitch: 1, // 转码开关
    transCount: 4, // 同时转码数
    transDeleted: true, // 转码完成是否删除源文件

    cryptTs: 1, // 是否加密(加密开关)
    cryptCount: 10, // 同时加密文件数

    sourceSavePath: 'movies', // 未转码文件存储文件夹
    tsSavePath: 'videos', // 转码后TS文件存储文件夹

    // 视频缩略图配置
    cutImg: {

        TYPES: {
            CUT_TYPE_CUSTOM: 'custom',
            CUT_TYPE_DEFAULT: 'default',

            FILE_TYPE_IMG: 'img',
            FILE_TYPE_GIF: 'gif'
        },

        onoff: true, // 是否开启截取视频缩略图
        type: 'custom', // 截取类型, custom: 自定义截取数据  default: 根据视频长度截取数据
        fileType: 'gif', // 截取的文件类型, img 为图片, gif 为动图, type 为custom时, 忽略该参数
        isFrameKey: false, // 是否以视频关键帧为准截取, 速度较慢, 但能截取到精彩部分, 如 type 为 custom, 则该参数无效
        filename: 'cut', // 输出文件名(不含后缀)
        // type 值为custom, 则data 的值类型必须为数组, default 值类型为 object
        // custom
        data: [
            {
                ss: 0, // 截取的开始时间
                t: 1,
                r: 1
            },
            {
                ss: 50, // 截取的开始时间
                t: 1,
                r: 1
            },
            {
                ss: 60, // 截取的开始时间
                t: 3,
                r: 15
            },
            {
                ss: 90, // 截取的开始时间
                t: 3,
                r: 15
            }
        ], 

        // default
        // data: {
        //     t: 3, // 截取的持续时间, fileType 为img时, t 与 r 的值必须为 1
        //     r: 15, // 截取的帧率, fileType 为img时, t 与 r 的值必须为 1
        //     count: 6 // 截取数量
        // }
    },


    uploadTs: 1, // 是否上传至云储存(云存储开关)
    uploadCount: 6, // 同时上传数量
    uploadDeleted: true, // 上传成功后是够删除源文件
    osses: [
        {
            type: 'minio',  // OSS类型：minio, ali, tengxun, qiniu
            name: '存储服务器1', // 备注
            sort: 1, // 按顺序上传(从小到大) 
            isuse: true,
            url: 'http://localhost', // 访问资源的域名
            endPoint: '127.0.0.1', // 对象存储服务的URL
            port: 9000, // 对象存储服务的端口号
            accessKey: 'minioadmin',
            secretKey: 'minioadmin',
            useSSL: false, // 是否使用 ssl
            bucket: 'test' // 存储桶名称
        },
        {
            type: 'ali',  // OSS类型：minio, ali, tencent, qiniu
            name: '存储服务器2', // 备注
            sort: 2, // 按顺序上传(从小到大) 
            isuse: false,
            url: 'https://laoyi-burger.oss-cn-shenzhen.aliyuncs.com',
            endPoint: 'oss-cn-shenzhen.aliyuncs.com', // 对象存储服务的URL
            accessKey: 'LTAIcIMnIKqK367H',
            secretKey: 'YgnbYFzCMtI0mLhxzn55JvwkwbTcVS',
            bucket: 'laoyi-burger' // 存储桶名称
        },
        {
            type: 'tencent',  // OSS类型：minio, ali, tencent, qiniu
            name: '存储服务器3', // 备注
            sort: 3, // 按顺序上传(从小到大) 
            isuse: false,
            url: 'https://yymh-1257284735.cos.ap-guangzhou.myqcloud.com',
            endPoint: 'ap-guangzhou', // 对象存储服务的URL
            accessKey: 'AKIDiBPREl55sQ3QYVfdh14uWsyoiYBY6tth',
            secretKey: 'hBQP7RbsteyDG3ZApRmumTu66rPh61vg',
            bucket: 'yymh-1257284735' // 存储桶名称
        },
    ],

    syncmovie: false, // 是否开启同步
    
    cmses: [
        {
            id: 'locahost:81',
            type: 'seacms', // CMS 类型：seacms 海洋cms, maccms 苹果cms
            name: '海洋视频站',
            isuse: true,
            host: '127.0.0.1',
            user: 'root',
            password: 'root',
            database: 'seacms',
            port: 3306
        },
        {
            id: 'locahost:80',
            type: 'maccms', // CMS 类型：seacms 海洋cms, maccms 苹果cms
            name: '苹果视频站',
            isuse: true,
            host: '127.0.0.1',
            user: 'root',
            password: 'root',
            database: 'maccms',
            port: 3306,
            prefix: 'mac_' // 数据表前缀
        }
    ],

    transfilename: 'index.m3u8', // 切片文件名(含后缀)
    transtssuffix: 'ts', // 切片后缀
    resloutons: [
        {
            chtitle: '清晰',
            title: '360',
            desc: '640x360'
        },
        {
            chtitle: '流畅',
            title: '480',
            desc: '852x480'
        },
        {
            chtitle: '720高清',
            title: '720',
            desc: '1280x720'
        },
        {
            chtitle: '1080高清',
            title: '1080',
            desc: '1920x1080'
        }
    ],
    seleresloutons: [
        {
            chtitle: '清晰',
            title: '480',
            desc: '852x480'
        },
        {
            chtitle: '流畅',
            title: '360',
            desc: '640x360'
        },
        {
            chtitle: '720高清',
            title: '720',
            desc: '1280x720'
        },
        {
            chtitle: '1080高清',
            title: '1080',
            desc: '1920x1080'
        }
    ],
    transFilter: {} 
}).write();

transingdb.defaults({
    data:[]
}).write();


module.exports = {
    transdb,
    transingdb
};