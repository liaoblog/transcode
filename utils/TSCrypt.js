const fs = require('fs')
const tsParse = require('./TSParse')

function TScrypt() {


}

TScrypt.prototype = {
    constructor: TScrypt,

    encryptV1: function (input, output, callback) {

        let ts = new tsParse(input)
        let tsData = ts.parse()
        let tsPackets = ts.getPackets().packets

        let count = tsData.length / 2
        let resCount = tsData.length / (count + 1)

        if (resCount >= 1) {
            // let encryIndexs = []
            for (let i = 0; i < count; i++) {
                let encryIndex = Math.floor(tsData.length * (resCount * (i + 1)) / tsData.length - 1)
                // encryIndexs.push(encryIndex)
                let packetData = tsData[encryIndex]
                let index = packetData['packetsIndex']
                for (let j = tsPackets[index][packetData['payloadIndex']] + 20; j < tsPackets[index].length; j++) {
                    tsPackets[index][j] = tsPackets[index][j] ^ 5
                }

            }
            // fs.writeFile('./test1.ts', Buffer.concat(ts.createTSPacket(encryIndexs).concat(tsPackets), tsPackets.length * 188), 'binary', function(err){
            fs.writeFile(output, Buffer.concat(tsPackets), 'binary', callback)
        }
    },


    decryptV1: function (input, output, callback) {
        this.encryptV1(input, output, callback)
    }




}

module.exports = new TScrypt()