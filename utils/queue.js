const { transdb } = require('../lowdb/index')

function Queue({ maxlength = 6 } = {}) {

    this.isRunning = false
    this.queues = []

    this.maxlength = maxlength

}

Queue.prototype = {

    constructor: Queue,

    // 添加任务并执行
    exec: function (task) {
        let self = this

        
        function execute() {

            self.isRunning = true
            if (self.queues.length > 0) {
                let task_exec = self.queues.shift()
                task_exec().finally(() => {
                    execute()
                })
                    
            } else {
                self.isRunning = false
            }

        }

        if (this.maxlength < 0 || this.queues.length < this.maxlength) {

            this.queues.push(task)
        }
        
        if (!this.isRunning) {
            execute()
        }

    },


    setMaxLength: function (len) {
        this.maxlength = len
        return this
    }



}

module.exports = {
    transQueue: new Queue({
        maxlength: (transdb.get('transCount').value())
    }),
    cryptQueue: new Queue({
        maxlength: (transdb.get('cryptCount').value())
    }),
    uploadQueue: new Queue(),
    syncQueue: new Queue(),
    deleteMovieFileQueue: new Queue({
        maxlength: -1
    })
}