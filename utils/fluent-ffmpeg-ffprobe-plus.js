require('fluent-ffmpeg/lib/ffprobe.js')  // 导入 ffprobe
const esprima = require('esprima')
const escodegen = require('escodegen')
const estraverse = require('estraverse')
const fs = require('fs')

const sourcePath = module.children[0].id  // 用 module 获取到 ffprobe 的路径（得先导入ffprobe）


// 修改后的代码的字符串
const newParseCode = `
    while (typeof line !== 'undefined') {
 
     if (line.match(/^\\[stream/i)) {
      var stream = parseBlock('stream');
       data.streams.push(stream);
    } else if (line.match(/^\\[chapter/i)) {
      var chapter = parseBlock('chapter');
      data.chapters.push(chapter);
     } else if (line.toLowerCase() === '[format]') {
       data.format = parseBlock('format');
    } else if (line.match(/^\\[[^\\/].*?/i)) {

       let name = line.slice(1,-1).toLowerCase()
       if(!data[name] || !(data[name] instanceof Array)) data[name] = []
      var res = parseBlock(name)
       data[name].push(res)
    }

    line = lines.shift();
   }
`

// 读取 ffprobe 的源代码
const oldParseCode = fs.readFileSync(sourcePath).toString()

// 将修改后的代码字符串转换为 AST
const newParseAST = esprima.parseScript(newParseCode)

// 将 ffprobe 源代码转换为 AST
var oldParseAST = esprima.parseScript(oldParseCode)

// 用 estraverse 找到 parseFfprobeOutput 函数的位置
estraverse.traverse(oldParseAST, {
    enter: (node) => {

        if (node.type == 'FunctionDeclaration' && node.id.name == 'parseFfprobeOutput') {

            // 再找到 while 循环的位置
            estraverse.replace(node, {
                enter: (node, parent) => {
                    if (node.type == 'WhileStatement' && parent.body.length > 4) {

                        // 直接将 while 循环位置的 AST 进行替换
                        return newParseAST
                    }
                }

            })


            return
        }
    }
})

// 用 escodegen 将 AST 转换为代码
const code = escodegen.generate(oldParseAST)

fs.writeFileSync(sourcePath, code)

