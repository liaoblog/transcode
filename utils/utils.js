const uuid = require('uuid')
const crypto = require('crypto')
const fs = require('fs')
const path = require('path')
const { transdb } = require('../lowdb/index')
const minio = require('../oss/minio')

function createUUID() {
    return uuid.v1().split("-").join('')
}

function getStrMd5(str) {
    const fsHash = crypto.createHash('md5')
    fsHash.update(str)
    const md5 = fsHash.digest('hex')
    return md5
}

function mkdirs(dirname) {
    if (fs.existsSync(dirname)) {
        return true;
    } else {
        if (mkdirs(path.dirname(dirname))) {
            fs.mkdirSync(dirname);
            return true;
        }
    }
}

function getSourceSavePath() {
    return path.join(__dirname, '..', transdb.get('sourceSavePath').value())
}

function getTsSavePath(movieid, definitionid) {
    return path.join(__dirname, '..', transdb.get('tsSavePath').value(), movieid, definitionid)
}

function packMovieUrl(obj = { type: 'current', url: transdb.get('siteurl').value(), port: 80, bucket: transdb.get('tsSavePath').value() }) {
    return JSON.stringify({
        type: obj.type,
        url: obj.url,
        port: obj.port,
        bucket: obj.bucket
    })
}

function packSyncUrl(url) {
    let preurl = `${url.url}${url.port == '80' ? '' : ':' + url.port}`
    if (url.type == 'current' || url.type == minio.OSS_TYPE) {
        preurl = `${preurl}/${url.bucket}`
    }
    return `${preurl}/${url.movieid}/${url.id}/${url.transfilename}`

}

function deleteNullDir(dirPath, stop) {
    if (fs.statSync(dirPath).isDirectory()) {
        let files = fs.readdirSync(dirPath)
        if (files.length <= 0) {
            if (dirPath.split(path.sep).pop() != stop) {
                fs.rmdirSync(dirPath)
                dirPath = path.join(dirPath, '..')
                deleteNullDir(dirPath)
            } else {
                return true
            }
        }
    }
    return

}

// true 大到小, false 小到大
function sortDefinition(flag = false) {
    return transdb.get('seleresloutons').value().sort(function (x, y) {
        x = parseInt(x.title)
        y = parseInt(y.title)
        if (x > y) {
            return flag ? -1 : 1
        } else if (x < y) {
            return flag ? 1 : -1
        }
        return 0
    })
}
// deleteNullDir(path.join(__dirname, '..', 'videos'), 'videos')
// console.log(path.join('D:\\nodejs\\transcodeServer\\transcode\\videos\\123\\321', '..'))
module.exports = {
    createUUID,
    getStrMd5,
    mkdirs,
    getSourceSavePath,
    getTsSavePath,
    deleteNullDir,
    packMovieUrl,
    sortDefinition,
    packSyncUrl
}