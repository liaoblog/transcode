const fs = require('fs')

class TSParse {

    constructor (source, {} = {}) {

        this._packets = []
        this._buff = []

        if (typeof source === 'string') {
            this._buff = fs.readFileSync(source)
        } else {
            this._buff = source
        }
        this._init()

    }

    _init() {
        let self = this
        let buf = self._buff
        let totalPacket = buf.length / 188
        for (let i=0;i<totalPacket;i++) {
            
            let packet = buf.slice(i*188,i*188+188)

            if (packet[0] == '0x47') {
          
                self._packets.push(packet)
            }

            
        }
    }

    getPackets() {
        return {
            totalPacket: this._packets.length,
            packets: this._packets
        }
    }

    getHeader (pid = '0x00', count = 'singl') {
        let self = this
        let packetsLen = self._packets.length
        let pats = []

        for (let i=0;i<packetsLen;i++) {

            let headerInfo = self.parseHeader(self._packets[i])

            if (headerInfo['pid'] == pid) {

                headerInfo['packetsIndex'] = i

                if (count == 'all') {
                    pats.push(headerInfo)
                } else if (count == 'singl'){
                    pats = headerInfo
                    break
                }
            }


        }

        return pats

    }

    parseHeader(buff) {

        let binData = null

        // 避免进行多次进制转换
        if (typeof buff === 'string') {
            binData = buff
        } else {
      
            binData = this._hexToBin(buff)
        }
         

        let payload_unit_start_indicator = this._BinToHex([binData.substr(9, 1)])
        let pid = this._BinToHex([binData.substr(11, 13)])

        let adaptation_field_control = this._BinToHex([binData.substr(26, 2)])

        let payloadIndex = 5

        let adaptation_field_length = '0x00'

        if (adaptation_field_control == '0x03') {
            adaptation_field_length = buff[4]
            payloadIndex = adaptation_field_length + 4 + 1

        }

        return {
            payload_unit_start_indicator,
            pid,
            adaptation_field_control,
            payloadIndex,
            adaptation_field_length,
            binData
        }


    }

    parsePAT (buff) {

        let headerInfo = this.parseHeader(buff)
        let binData = headerInfo['binData']

        let s = headerInfo['payloadIndex'] * 8
        
        let res = []

        for (let i=0;i>=0;i++) {

            let signlfor = 32
            let crc = 32

            let program_number = this._BinToHex([binData.substr(s + 64 + signlfor * i, 16)])
            let pmt_pid = this._BinToHex([binData.substr(s + 64 + 16 + 3 + signlfor * i, 13)])

            res.push({
                program_number,
                pmt_pid
            })

            let endByte = this._BinToHex([binData.substr(s + 64 + (signlfor * (i + 1)) + crc, 8)])
            if (endByte == '0xff') {

                break

            }

        }
        return res
        

    }

    parsePMT (buff) {

        let headerInfo = this.parseHeader(buff)
        let binData = headerInfo['binData']

        let s = headerInfo['payloadIndex'] * 8


        let res = []

        for (let i=0;i>=0;i++) {

            let signlfor = 40
            let crc = 32

            let stream_type = this._BinToHex([binData.substr(s + 96 + signlfor * i, 8)])
            let elementary_PID = this._BinToHex([binData.substr(s + 96 + 8 + 3 + signlfor * i, 13)])

            res.push({
                stream_type,
                elementary_PID
            })

            let endByte = this._BinToHex([binData.substr(s + 64 + (signlfor * (i + 1)) + crc, 8)])
            if (endByte == '0xff') {

                break

            }

        }
        return res

    }

    parsePlus() {

        let self = this
        let packetsLen = self._packets.length

        let patP, patPBin, pmtP, pmtBin, dataPid = null

        let res = []

        for (let i=0;i<packetsLen;i++) {

            let headerInfo = self.parseHeader(self._packets[i])

            if (patP==null && headerInfo['pid'] == '0x00') {

                patPBin = this._hexToBin(self._packets[i])
                patP = self.parsePAT(patPBin)
                
                if (patP.length > 0 && patP[0]['program_number'] == '0x01') {
                    patP = patP[0]['pmt_pid']
                }

            }

            if (pmtP==null && headerInfo['pid'] == patP) {

                pmtBin = this._hexToBin(self._packets[i])
                pmtP = self.parsePMT(pmtBin)
                if (pmtP.length > 0) {
                    pmtP.forEach( ele => {
                        if (ele['stream_type'] == '0x1b') {
                            pmtP = ele
                            return
                        }
                    })
                }
                dataPid = pmtP['elementary_PID']
            }

            
            if (dataPid!=null && headerInfo['pid'] == dataPid) {
                res.push(headerInfo)

            }


        }

        return res

    }

    parse() {
        let self = this
        
        let patInfo = self.parsePAT(self.getHeader()['binData'])

        if (patInfo.length > 0 && patInfo[0]['program_number'] == '0x01') {
            patInfo = patInfo[0]['pmt_pid']
        }

        let pmtInfo = self.parsePMT(self.getHeader(patInfo)['binData'])

        if (pmtInfo.length > 0) {
            pmtInfo.forEach( ele => {
                if (ele['stream_type'] == '0x1b') {
                    pmtInfo = ele
                    return
                }
            })
        }

        let res = []

        self.getHeader(pmtInfo['elementary_PID'], 'all').forEach( ele => {
            if (ele.payload_unit_start_indicator == '0x01') {
                res.push(ele)
            }
        })

        return res
    }


    createTSPacket (data) {

        let res = []
        let maxDataLen = (188 - 6) / 2


        let createHeader = function () {
            let header = Buffer.alloc(188)
            header.fill(0xff)
            header[0] = '0x47'
            header[1] = '0x40'
            header[2] = '0x11'
            header[3] = '0x10'
            header[4] = '0x00'
            header[5] = Math.floor(data.length / maxDataLen) + 1

            return header
        }

        let header = createHeader()
        let j = 0
        data.forEach( (ele, i) => {
            ele = ele.toString(2)
            
            if (ele.length < 16) {
                let l = ele.length
                for(let i=0;i<(16 - l);i++) {
                    ele = '0' + ele
                }
            }
            
            header[6 + j * 2] = parseInt(ele.substr(0, 8), 2)
            header[6 + j * 2 + 1] = parseInt(ele.substr(8), 2)


            if ((j + 1 * 2) > maxDataLen) {
                res.push(header)
                header = createHeader()
                j = 0
            } else {
                j++
            }
            

        })

        res.push(header)


        return res


    }


    _BinToHex (data) {
        let d = []
        data.forEach(ele => {
            let r = parseInt(ele, 2).toString(16)
            if (r.toString().length < 2) {
                let l = r.length
                for(let i=0;i<(2 - l);i++) {
                    r = '0' + r
                }
            }
            r = '0x' + r
            d.push(r)
        });
        return d.join('')
    }

    _hexToBin (data) {
        let d = []
  
        data.forEach(ele => {
            let r = ele.toString(2)
            if (r.toString().length < 8) {
                let l = r.length
                for(let i=0;i<(8 - l);i++) {
                    r = '0' + r
                }
            }
            d.push(r)
        });
        return d.join('')

    }

}

module.exports = TSParse