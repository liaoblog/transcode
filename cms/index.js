const Sequelize = require('sequelize');
const { transdb } = require('../lowdb/index')

function CMS() {

    this.cmsIntances = {}

    this.init()


}
CMS.prototype = {
    constructor: CMS,

    TYPE: {
        SEACMS: 'seacms',
        MACCMS: 'maccms'
    },

    STATUS: {
        SYNC_FAIL: 4,
        SYNC_SUCC: 2
    },

    init: function () {

        let cmses = transdb.get('cmses').value()

        cmses.forEach(v => {
            if (v.isuse) {
                this.cmsIntances[v.id] = {
                    ins: new Sequelize(v.database, v.user, v.password, {
                        host: v.host,
                        dialect: 'mysql',
                        port: v.port,
                        pool: {
                            max: 10,
                            min: 0,
                            acquire: 30000,
                            idle: 10000
                        },
                        timezone: '+08:00',
                        dialectOptions: {
                            dateStrings: true,
                            typeCast: true
                        },
                        logging: false
                    }),
                    type: v.type,
                    name: v.name,
                    source: v
                }
            }
        })
    },

    getInstances: function () {
        return this.cmsIntances
    },


    sync: async function ({
        videoName,
        collName,
        index,
        url,
        succCallback,
        failCallback,
        startCallback
    }) {
        if (startCallback) await startCallback()

        Object.keys(this.cmsIntances).forEach(async v => {
            let dbObj = this.cmsIntances[v]

            // 海洋cms 同步
            if (dbObj.type == this.TYPE.SEACMS) {

                const PLAYER = 'Dplayer'
                const CASEPLAYER = 'dp'


                try {
                    let res = await dbObj.ins.query('select * from sea_data where v_name = :videoName', {
                        replacements: {
                            videoName
                        },
                        type: Sequelize.QueryTypes.SELECT
                    })
                    if (res.length > 0) {
                        res = res[0]
                        let v_id = res['v_id']

                        let urlRes = await dbObj.ins.query('select * from sea_playdata where v_id = :v_id', {
                            replacements: {
                                v_id
                            },
                            type: Sequelize.QueryTypes.SELECT,
                            plain: true
                        })


                        let urls = []
                        if (urlRes['body'].length > 0) {
                            urls = urlRes['body'].split('$$')[1].split('#')
                        }

                        --index < 0 ? 0 : index

                        urls[index] = collName + '$' + url.movieurl + '$' + CASEPLAYER
                        urls = PLAYER + '$$' + urls.join('#')


                        await dbObj.ins.query('update sea_playdata set body = :body where v_id = :v_id', {
                            replacements: {
                                v_id,
                                body: urls
                            }
                        })

                        if (succCallback) succCallback(this._packSyncMsg(dbObj, this.STATUS.SYNC_SUCC))



                    } else {
                        throw new Error('video not found')
                    }
                } catch (err) {
                    if (failCallback) failCallback(this._packSyncMsg(dbObj, this.STATUS.SYNC_FAIL, err.message))
                }


            }

            // 苹果cms 同步
            if (dbObj.type == this.TYPE.MACCMS) {

                try {
                    const PLAYER = 'dplayer'

                    let tablePrefix = dbObj.source.prefix

                    let res = await dbObj.ins.query(`select * from ${tablePrefix}vod where vod_name = :videoName`, {
                        replacements: {
                            videoName
                        },
                        type: Sequelize.QueryTypes.SELECT,
                        plain: true
                    })
                    if (res) {

                        let urls = []
                        if (res['vod_play_url'].length > 0) {
                            urls = res['vod_play_url'].split('#')
                        }

                        --index < 0 ? 0 : index

                        urls[index] = collName + '$' + url.movieurl
                        urls = urls.join('#')

                        await dbObj.ins.query(`update ${tablePrefix}vod set vod_play_url = :vod_play_url, vod_play_from = :vod_play_from, vod_pic_screenshot = :vod_pic_screenshot where vod_id = :vod_id`, {
                            replacements: {
                                vod_id: res.vod_id,
                                vod_play_url: urls,
                                vod_pic_screenshot: url.screenhost,
                                vod_play_from: PLAYER
                            }
                        })

                        if (succCallback) succCallback(this._packSyncMsg(dbObj, this.STATUS.SYNC_SUCC))

                    } else {
                        throw new Error('video not found')
                    }
                } catch (err) {
                    if (failCallback) failCallback(this._packSyncMsg(dbObj, this.STATUS.SYNC_FAIL, err.message))
                }

            }


        })

    },

    _packSyncMsg: function (dbObj, status, msg) {
        return {
            id: dbObj.source.id,
            msg: `${status}:${msg?msg:''}`,
            type: dbObj.type,
            name: dbObj.name
        }
    }



}

module.exports = new CMS()